src package
===========

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   src.shapes

Submodules
----------

src.math\_func module
---------------------

.. automodule:: src.math_func
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src
   :members:
   :undoc-members:
   :show-inheritance:
