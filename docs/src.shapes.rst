src.shapes package
==================

Submodules
----------

src.shapes.circle module
------------------------

.. automodule:: src.shapes.circle
   :members:
   :undoc-members:
   :show-inheritance:

src.shapes.point module
-----------------------

.. automodule:: src.shapes.point
   :members:
   :undoc-members:
   :show-inheritance:

src.shapes.rect module
----------------------

.. automodule:: src.shapes.rect
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.shapes
   :members:
   :undoc-members:
   :show-inheritance:
