# circle class

class circle():
    '''
    Circle object
    Default -> radius=1
    
    '''
    def __init__(self, radius=1):
        '''
        Intialization of circle class.
        
        '''

        self.radius=radius


    def area(self):
        '''
        Area of the circle object

        Pre:
            Circle object
        
        Post:
            area (float)
        
        Formula:
            area = 3.14 * radius**2
        
        '''

        return 3.14 * self.radius**2


    def perimeter(self):
        '''
        Perimeter of the circle object

        Pre:
            Circle object
        
        Post:
            perimeter (float)
        
        Formula: perimeter = 2 * 3.14 * radius
        
        '''

        return 2 * 3.14 * self.radius

    
