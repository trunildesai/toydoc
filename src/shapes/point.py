# point class


class point():
    '''

    Class point with coordinate x and y in two dimensions.
    
    Default position: x=0, y=0
    
    '''
    def __init__(self, x=0, y=0):

        '''
        Intialization of point class.
        
        '''

        self.x = x
        self.y = y

    def __str__(self):

        return f'x={self.x}, y={self.y}'

    
    def distanceFrom(self, other):
        '''

        Calculate distance between point object and other point object.

        Pre:
            other = point object from which distance of self is to be calculated.
                
        Post:
            Distance (float)
        
        '''

        distance = ((other.x - self.x)**2 + (other.y - self.y)**2)**0.5

        return distance
        

        

        
