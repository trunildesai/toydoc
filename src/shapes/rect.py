# rectangle class

import point

class rectangle():
    '''
    Rectanlge class:

    Default:
        width = 1
        height = 1
        corner = point(x=0,y=0)

    Assumes the rectangle base parallel to X-axis.
    
    '''
        
    def __init__(self, width=1, height=1, corner = point.point(0,0)):

        '''
        Initialization of rectangle class.
        
        '''

        self.width = width
        self.height = height
        self.corner = corner


    def centre(self):
        '''
        
        Find the centre point of the rectangle.

        Pre:
            Rectangle object (self)
        
        Post:
            Point object indicating position of the central point.
    
        '''
        x = self.width/2
        y = self.height/2

        return point.point(x,y)
        

    def area(self):
        '''
        Area of the rectangle object.

        Pre:
            Rectangle object (self)
        
        Post:
            Area of the rectangle (float)

        
        '''
        
        return self.width * self.height

    def perimeter(self):
        '''
        Perimeter of the rectangle object
        
        Pre:
            Rectangle object (self)
        
        Post:
            Perimeter of the rectangle (float)
        
        '''

        return 2 * self.width + 2 * self.height

    
